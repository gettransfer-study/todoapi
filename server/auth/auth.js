const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const JWTstrategy = require("passport-jwt").Strategy;
// We use this to extract the JWT sent by the user
const ExtractJWT = require("passport-jwt").ExtractJwt;
const { User, } = require("../models");

// Create a passport middleware to handle user registration
passport.use("signup", new LocalStrategy({
  usernameField: "email",
  passwordField: "password",
}, async (email, password, done) => {
  try {
    // Save the information provided by the user to the the database
    const existingUser = await User.findOne({ where: { email, }, });
    if (existingUser) {
      return done(false, { error: "User exists", });
    }
    const user = new User();
    user.email = email;
    user.password = await user.generateHash(password);
    const savedUser = await user.save();
    // Send the user information to the next middleware
    return done(null, savedUser);
  }
  catch (error) {
    return done(error);
  }
}));

// Create a passport middleware to handle User login
passport.use("login", new LocalStrategy({
  usernameField: "email",
  passwordField: "password",
}, async (email, password, done) => {
  try {
    // Find the user associated with the email provided by the user
    const user = await User.findOne({ where: { email, }, });
    if (!user) {
      // If the user isn't found in the database, return a message
      return done(false, { message: "User not found", });
    }
    // Validate password and make sure it matches with the corresponding hash stored in the database
    // If the passwords match, it returns a value of true.
    const validate = await user.isValidPassword(password);
    if (!validate) {
      return done(false, { message: "Wrong Password", });
    }
    // Send the user information to the next middleware
    return done(user, { message: "Logged in Successfully", });
  }
  catch (error) {
    return done(error);
  }
}));

// This verifies that the token sent by the user is valid
passport.use(new JWTstrategy({
  // secret we used to sign our JWT
  secretOrKey: "top_secret",
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
}, async (token, done) => {
  try {
    // Pass the user details to the next middleware
    return done(null, token.user);
  }
  catch (error) {
    return done(error);
  }
}));
