const express = require("express");
const todosController = require("../controllers").todos;

const router = express.Router();

router.post("/todos", todosController.create);
router.get("/todos", todosController.index);
router.put("/todos/:id", todosController.put);
router.delete("/todos/:id", todosController.delete);

module.exports = router;
