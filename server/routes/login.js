const express = require("express");
const passport = require("passport");
const jwt = require("jsonwebtoken");

const router = express.Router();

router.get("/api", (req, res) => res.status(200).send({
  message: "Welcome to the Todos API!",
}));

router.post("/signup", passport.authenticate("signup", { session: false, }), async (req, res) => {
  if (req.user.error) {
    return res.status(400).send({ error: req.user.error, });
  }
  res.status(202).send({
    message: "Signup successful",
    user: {
      email: req.user.email,
    },
  });
});

router.post("/login", async (req, res, next) => {
  passport.authenticate("login", async (user, data) => {
    try {
      if (!user) {
        return res.status(400).json({ error: data.message, });
      }
      return req.login(user, { session: false, }, async (error) => {
        if (error) return next(error);
        // We don't want to store the sensitive information such as the
        // user password in the token so we pick only the email and id
        const body = { id: user.id, email: user.email, };
        // Sign the JWT token and populate the payload with the user email and id
        const token = jwt.sign({ user: body, }, "top_secret");
        // Send back the token to the user
        return res.json({ token, });
      });
    }
    catch (error) {
      return next(error);
    }
  })(req, res, next);
});

module.exports = router;
