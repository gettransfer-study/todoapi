const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define("User", {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {});
  User.associate = function associate(models) {
    User.hasMany(models.Todo, {
      foreignKey: "userId",
      sourceKey: "id",
    });
  };

  User.prototype.generateHash = function generateHash(password) {
    return bcrypt.hash(password, bcrypt.genSaltSync(8));
  };

  User.prototype.isValidPassword = function isValidPassword(password) {
    return bcrypt.compare(password, this.password);
  };
  return User;
};
