module.exports = (sequelize, DataTypes) => {
  const Todo = sequelize.define("Todo", {
    text: DataTypes.STRING,
    color: DataTypes.STRING,
    time: DataTypes.DATE,
    urgent: DataTypes.STRING,
    image: DataTypes.STRING,
    done: DataTypes.BOOLEAN,
  }, {});
  Todo.associate = (models) => {
    Todo.belongsTo(models.User, {
      foreignKey: "userId",
      onDelete: "CASCADE",
    });
  };
  return Todo;
};
