const { Todo, } = require("../models");

module.exports = {
  async create(req, res) {
    try {
      const newTodo = await Todo.create({ ...req.body.todo, userId: req.user.id, });
      return res.status(201).send(newTodo);
    }
    catch (err) {
      return res.status(400).send(err);
    }
  },
  async index(req, res) {
    try {
      const todos = await Todo.findAll({ where: { userId: req.user.id, }, });
      return res.status(200).send(todos);
    }
    catch (err) {
      return res.status(400).send(err);
    }
  },
  async put(req, res) {
    try {
      const todo = await Todo.findOne({
        where: {
          id: req.params.id,
          userId: req.user.id,
        },
      });
      const newTodo = await todo.update({ ...req.body.todo, });
      return res.status(200).send(todo);
    }
    catch (err) {
      return res.status(400).send(err);
    }
  },
  async delete(req, res) {
    try {
      const todo = await Todo.findOne({
        where: {
          id: req.params.id,
          userId: req.user.id,
        },
      });
      await todo.destroy();
      return res.status(204).send("");
    }
    catch (err) {
      return res.status(400).send(err);
    }
  },
};
