const express = require("express");
const passport = require("passport");
const logger = require("morgan");
const bodyParser = require("body-parser");
const cors = require('cors');
require("./server/auth/auth");


// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger("dev"));

app.use(cors());

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false, }));

const loginRoutes = require("./server/routes/login");
const todosRoutes = require("./server/routes/todos");

app.use("/", loginRoutes);
app.use("/api", passport.authenticate("jwt", { session: false, }), todosRoutes);
// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get("*", (req, res) => res.status(200).send({
  message: "Welcome to the beginning of nothingness.",
}));

module.exports = app;
